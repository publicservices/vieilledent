export default {
    label: 'Address Email',
    name: 'address_email',
    widget: 'text',
    required: false,
    hint: 'The email address, associated with this physical address'
}
