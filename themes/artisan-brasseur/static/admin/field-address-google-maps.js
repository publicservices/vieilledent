export default {
    label: 'Address Google Maps',
    name: 'address_google_maps',
    widget: 'text',
    required: false,
    hint: 'The URL address, of the google maps page, that represent this physical address/business'
}
