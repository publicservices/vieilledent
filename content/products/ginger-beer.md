+++
title = "Ginger Beer"
image = "/media/uploads/matriochkas-5-gigognes.jpeg"
date_published = "2020-12-03"
bottle_size = "33cl"
alcohol = "4.5°"
ingredients = "Eau, malt d'orge, mais, houblon, sucre, levure"
+++

Ginger Beer
