export default {
    label: 'Address Street Name',
    name: 'address_street_name',
    widget: 'text',
    required: false,
    hint: 'The street name pointing to this address'
}
