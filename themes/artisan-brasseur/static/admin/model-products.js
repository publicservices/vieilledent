import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import datePublished from './field-date-published.js'
import bottleSize from './field-bottle-size.js'
import alcohol from './field-alcohol.js'
import ingredients from './field-ingredients.js'

const products  = {
    name: 'products',
    label: 'Products',
    label_singular: 'Product',
    folder: 'content/products',
    format: 'toml-frontmatter',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	datePublished,
	body,
	alcohol,
	bottleSize,
	ingredients,
    ]
}

export default products
