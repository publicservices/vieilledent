const documentReady = () => {
    if (document.readyState === 'complete') {
	initWebsite()
    }
}

const initWebsite = () => {
    console.log('Brasserie Vieilledent!')
}

document.onreadystatechange = documentReady
