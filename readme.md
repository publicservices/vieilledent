# Portfolio

The portoflio website can be:
- seen live here https://publicservices.frama.io/vieilledent
- edited by an admin (a member of the gitlab project): https://publicservices.frama.io/vieilledent/admin

## Custom domain name

To have a custom domain name (https://vieilledent.net), customize the settings in Framagit (instance of open source software Gitlab).

# Technical aspects

## Overview

Quick project structure:

- `./config.toml` config of the site, its title (name), the theme it uses and some parameters we are using accross the site.
- `./content/` all content files for our site. Folder and structure that represent our different models, and the content for all pages.
- `./static/media/uploads/`, where we put or upload images that we want to reference in the content.
- `./themes/`, all themes this site can use. This is where the templates for pages and components re-used accross the site are defined.
= `./themes/portfolio-index/`, the theme we're currently using, all files for its definition and configuration.
- `./gitlab-ci.yml`, gitlab's continous deployment file describing jobs, such as deploying the website to the server.

All folders at the root level, which have the same name name as folders in the theme level, are used to overwritte files found in the later. Here, since we're developing a theme specifically for this site, `./content` is the only folder that matters at root level.

## Development

If you want to edit, the content, or the structure, of the site locally:
- install [gohugo.io](https://gohugo.io/)
- run `hugo server` inside this project.

## Deloyment

Currently we're using gitlab pages (which can also be used with a custom domain), configured through the `./gitlab-ci.yml` file. The `pages` job is the one that describe the process.

Building the site, will create a folder ready to be copied for live deployment on a (HTTPS) server, just run `hugo`, to generate the `./public` folder.
