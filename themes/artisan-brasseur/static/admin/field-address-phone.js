export default {
    label: 'Address Phone',
    name: 'address_phone',
    widget: 'text',
    required: false,
    hint: 'The phone number, associated with this physical address.'
}
