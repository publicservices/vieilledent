+++
title = "American Dark Lager"
image = "/media/uploads/maison-toulouse-neutre.jpeg"
date_published = "2020-12-01"
bottle_size = "33cl"
alcohol = "4.5°"
ingredients = "Eau, malt d'orge, mais, houblon, sucre, levure"
+++

American Dark Lager
