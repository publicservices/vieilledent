import collections from './collections.js'

export default {
    config: {
	// Skips config.yml.
	// By not skipping, the configs will be merged, with the js-version taking priority.
	load_config_file: false,
	display_url: window.location.origin,

	backend: {
	    name: 'gitlab',
	    repo: 'publicservices/vieilledent',
	    auth_type: 'implicit',
	    app_id: 'c2a2f094e622ce15069e9997f92b31b805d62a9c4c4f99c4510eb991ba171e64',
	    api_root: 'https://framagit.org/api/v4',
	    base_url: 'https://framagit.org',
	    auth_endpoint: 'oauth/authorize',
	    branch: 'master',
	},

	media_folder: 'static/media/uploads',
	public_folder: '/media/uploads',

	collections
    }
}
