export default {
    label: 'Alcohol',
    name: 'alcohol',
    widget: 'text',
    required: false,
    hint: 'The alcohol level in degrees for this item; ex: 4.5°'
}
