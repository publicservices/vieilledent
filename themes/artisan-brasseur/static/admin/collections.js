import products from './model-products.js'
import pages from './model-pages.js'
import configs from './model-configs.js'
import datas from './model-datas.js'

export default [
    products,
    pages,
    datas,
    configs
]
