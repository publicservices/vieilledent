export default {
    label: 'Address Street Number',
    name: 'address_street_number',
    widget: 'text',
    required: false,
    hint: 'The street number for this address'
}
