export default {
    label: 'Address Postal Code',
    name: 'address_postal_code',
    widget: 'text',
    required: false,
    hint: 'The postal code associated with this address'
}
