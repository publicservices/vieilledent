export default {
    label: 'Address City',
    name: 'address_city',
    widget: 'text',
    required: false,
    hint: 'In which city is the located the address'
}
