export default {
    label: 'Address Country',
    name: 'address_country',
    widget: 'text',
    required: false,
    hint: 'In which country is located the address'
}
