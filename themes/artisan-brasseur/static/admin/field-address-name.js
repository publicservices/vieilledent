export default {
    label: 'Address Mame',
    name: 'address_name',
    widget: 'text',
    required: false,
    hint: 'The Name, on doorbell at this address'
}
