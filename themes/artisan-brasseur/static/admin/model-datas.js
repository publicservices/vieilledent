import addressCity from './field-address-city.js'
import addressCountry from './field-address-country.js'
import addressEmail from './field-address-email.js'
import addressGoogleMaps from './field-address-google-maps.js'
import addressName from './field-address-name.js'
import addressPhone from './field-address-phone.js'
import addressPostalCode from './field-address-postal-code.js'
import addressStreetName from './field-address-street-name.js'
import addressStreetNumber from './field-address-street-number.js'

const datas = {
    name: 'data',
    label: 'Datas',
    editor: {
	preview: false
    },
    files: [
	/* {
	   label: 'Homepage',
	   name: 'homepage',
	   file: 'data/homepage.yml',
	   fields: [
	   featuredImage,
	   body
	   ]
	   }, */
	{
	    label: 'Contact',
	    name: 'contact',
	    file: 'data/contact.yml',
	    fields: [
		addressName,
		addressStreetName,
		addressStreetNumber,
		addressPostalCode,
		addressCity,
		addressCountry,
		addressEmail,
		addressPhone,
		addressGoogleMaps,
	    ]
	}
	
    ]
}

export default datas
