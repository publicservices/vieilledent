export default {
    label: 'Bottle size',
    name: 'bottle_size',
    widget: 'text',
    required: false,
    hint: 'The size of the bottle; ex: 33cl'
}
