export default {
    label: 'Ingredients',
    name: 'ingredients',
    widget: 'text',
    required: false,
    hint: 'Write the list of ingredients for this item.'
}
